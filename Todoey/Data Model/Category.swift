//
//  Category.swift
//  Todoey
//
//  Created by Taylor Potts on 2020-10-27.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    let items = List<Item>()
    @objc dynamic var cellColor: String = ""
}
